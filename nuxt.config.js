const env = require('./secrets.json').NODE_ENV;
const { resolve } = require('path')

module.exports = {
  /*server: {
    port: 3000, // default: 3000
    host: '93.188.167.68', // default: localhost
  },*/
  mode: 'universal',
  build: {
    vendor: ['axios','vue-i18n'],
    publicPath: `/${env}/_nuxt/`,
  },
  srcDir: 'client/',
  performance: {
    gzip: false,
  },
  router: {
    base: '/',
    middleware: 'i18n',
  },
  plugins: [
    {src: '~plugins/i18n.js', injectAs: 'i18n'},
    {src: '~plugins/vue-lazyload.js', ssr: false},
    //{ src: '~/plugins/localStorage.js', ssr: false }
  ],
  dev: false,
  buildDir: resolve(__dirname, '.nuxt'),
  rootDir: __dirname,

  serverMiddleware: ['../api/auth'],
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    ['nuxt-validate', {lang: 'es'}],
    //'vue-lazyload'
    ['@nuxtjs/google-tag-manager', { id:'GTM-MNZZNT6'}],
  ],
  axios: {
    //'/api': 'http://localhost:3000',
    //baseURL:'http://localhost:3000',
    baseURL:'https://pbpbz9qnzg.execute-api.ap-northeast-1.amazonaws.com/dev/',
    //proxy: true
  },
  proxy: {
  },
  env: {
    s3_bucket_url: "https://s3-ap-northeast-1.amazonaws.com/good-forest-files-upload-prod/",
    upload_signature: "https://pbpbz9qnzg.execute-api.ap-northeast-1.amazonaws.com/dev/",
    app_url: "https://goodforest.domiearth.com",
  },
  head: {
    title: 'Good forest',
    titleTemplate: '%s | Good forest',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Good Forest project' },
      //{ "http-equiv": 'Content-Security-Policy', content:"default-src * 'self' data: 'unsafe-inline' 'unsafe-eval' *"  },
    ],
  },
  css: [
    // CSS file in the project
    '@/assets/css/bootstrap.css',
    '@/assets/css/font-awesome.min.css',
    '@/assets/css/vll_main.css',
  ],
  auth: {
    redirect: {
      callback: '/callback'
    },
    strategies: {
      local: {
        endpoints: {
          login: { propertyName: 'token.accessToken' }
        }
      }
    }
  }

};
