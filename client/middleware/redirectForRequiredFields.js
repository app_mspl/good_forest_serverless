
export default function ({ store, redirect }) {
  // If the user is not filled required fields


  if ((!store.state.apply.form_data.basic_info.tax_id) || (!store.state.apply.form_data.basic_info.email) || (!store.state.apply.form_data.basic_info.city) || (!store.state.apply.form_data.basic_info.independent_electrical)) {
    return redirect('/apply/basic-information')
  }
  //return redirect('/apply/basic-information')
}