import Vue from 'vue';
import createPersistedState from 'vuex-persistedstate'

Vue.use(createPersistedState)

export default ({store}) => {
  window.onNuxtReady(() => {
    createPersistedState()(store)
    /*createPersistedState({
      storage: {
        getItem: key => Cookies.get(key),
        // Please see https://github.com/js-cookie/js-cookie#json, on how to handle JSON.
        setItem: (key, value) =>
          Cookies.set(key, value, { expires: 3, secure: true }),
        removeItem: key => Cookies.remove(key),
      },
    })(store)*/
  })
}
