import Vue from 'vue';
import VueLazyload from 'vue-lazyload';


Vue.use(VueLazyload)


let VueUploadMultipleImage

if (process.client) {
	VueUploadMultipleImage = require('vue-upload-multiple-image')

  	Vue.use(VueUploadMultipleImage, {name: 'vue-upload-multiple-image'})
}


/*
if (process.BROWSER_BUILD) {
	
	import VueUploadMultipleImage from 'vue-upload-multiple-image'
}*/

export default VueLazyload;
