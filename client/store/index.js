export const state = () => ({
  locales: [
    {
      code: 'zh',
      name: '中文'
    },
    {
      code: 'en',
      name: 'EN'
    },
  ],
  locale: 'zh'
});

export const mutations = {
  SET_LANG(state, locale) {
    if (state.locales.find(el => el.code === locale)) {
      state.locale = locale
    }
  }
};