//export const strict = false

export const state = () => ({
  form_data: {
    'redirect_review':false,
    'disqualify_reason_key':"",
    'basic_info':{tax_id:"",company_name:"",name:"",national_id:"",email:"",main_phone:"",
      mobile_phone:"",city:"",district:"",address:"",
      independent_electrical:"",installation_location:"",already_participated:"",
      taipower_primary_account:"",
      taipower_secondary_accounts:[],
      taipower_bills_images:[],
      license_images:[],
      rental_agreement_images:[],
      auth_completed_images:[],
      interior:[],
      exterior:[],
      electrical_panel_quantity:"",
      electrical_panel_distance:"",
      referral:"",
      referral_partner:"",
      lighting_color:"",
      lights:{},
      video:{url:'',buffer:'',video:[]},
      status:"draft",
      user_id:false,
    },
  },
  list: [
    {key:'grille_light',selected:false,'ref_image':'grille_light_img'},
    {key:'mountain_light',selected:false,'ref_image':'mountain_light_img'},
    {key:'middle_east_light',selected:false,'ref_image':'middle_east_light_img'},
    {key:'bracket_laminate_lamp',selected:false,'ref_image':'bracket_laminate_lamp_img'},
    {key:'worklight',selected:false,'ref_image':'worklight_img'}
  ],
  notification_data: {
      user_id:"",
      createdAt:"",
      email:"",
      main_phone:"",
      social_type:""    
  },

})

export const mutations = {
  redirect_toggle (state, { value }) {
    state.form_data.redirect_review=value
  },
  toggle_status (state, { value }) {
    state.form_data.basic_info.status=value
  },
  add_disqualify_reason_key (state, { key }) {
    state.form_data.disqualify_reason_key=key
  },
  add_basic_info (state, payload) {
    state.form_data.basic_info.tax_id=payload.tax_id
    state.form_data.basic_info.company_name=payload.company_name
    state.form_data.basic_info.name=payload.name
    state.form_data.basic_info.national_id=payload.national_id
  },
  add_contact_info (state, payload) {
    state.form_data.basic_info.email=payload.email
    state.form_data.basic_info.main_phone=payload.main_phone
    state.form_data.basic_info.mobile_phone=payload.mobile_phone
  },
  add_install_info (state, payload) {
    state.form_data.basic_info.city=payload.city
    state.form_data.basic_info.district=payload.district
    state.form_data.basic_info.address=payload.address
  },
  add_more_info (state, payload) {
    state.form_data.basic_info.independent_electrical=payload.independent_electrical
    state.form_data.basic_info.installation_location=payload.installation_location
    state.form_data.basic_info.already_participated=payload.already_participated
  },
  add_account_info (state, payload) {
    state.form_data.basic_info.taipower_primary_account=payload.taipower_primary_account
  },
  add_taipower_secondary_accounts (state, payload) {
    state.form_data.basic_info.taipower_secondary_accounts=payload.taipower_secondary_accounts
  },
  add_taipower_bill_upload (state, payload) {
    state.form_data.basic_info.taipower_bills_images=payload.images
  },
  add_license_images_upload (state, payload) {
    state.form_data.basic_info.license_images=payload.images
  },
  add_rental_agreement_images_upload (state, payload) {
    state.form_data.basic_info.rental_agreement_images=payload.images
  },
  add_auth_completed_images_upload (state, payload) {
    state.form_data.basic_info.auth_completed_images=payload.images
  },  
  add_interior_upload (state, payload) {
    state.form_data.basic_info.interior=payload.images
  },
  add_exterior_upload (state, payload) {
    state.form_data.basic_info.exterior=payload.images
  },

  add_electrical_panel_data (state, payload) {
    state.form_data.basic_info.electrical_panel_quantity=payload.electrical_panel_quantity
    state.form_data.basic_info.electrical_panel_distance=payload.electrical_panel_distance
  },
  
  add_video_url(state, payload) {
    state.form_data.basic_info.video.url=payload.url
  },
  add_video_buffer(state, payload) {
    state.form_data.basic_info.video.buffer=payload.buffer
    state.form_data.basic_info.video.video=payload.video
  },
  add_referral (state, payload) {
    state.form_data.basic_info.referral=payload.referral
  },
  add_referral_partner (state, payload) {
    state.form_data.basic_info.referral_partner=payload.referral_partner
  },
  add_lighting_color (state, payload) {
    state.form_data.basic_info.lighting_color=payload.lighting_color
  },
  
  clear_state_data (state, payload) {
    for (var key in state.form_data.basic_info ) {

      if(key == 'video'){

        state.form_data.basic_info[key].url=""
        state.form_data.basic_info[key].buffer=""
        state.form_data.basic_info[key].video=[]

      }else if(key == 'status' ){
        state.form_data.basic_info[key]='draft'
      }
      else if(typeof state.form_data.basic_info[key] == 'object' ){
        state.form_data.basic_info[key]={}
      }
      else if(typeof state.form_data.basic_info[key] == 'boolean' ){
        state.form_data.basic_info[key]=false
      }else{
        state.form_data.basic_info[key]=""
      }
      
    }
  },
  add_lights (state, payload) {
    state.form_data.basic_info.lights[payload.key]={'counter':payload.counter,'images':payload.images}
    state.list[payload.index].selected=true
  },
  delete_lights (state, { light_type }) {
    let i=0;

    for (i = 0; i < state.list.length; ++i) {
      if(state.list[i].key == light_type){
        state.list[i].selected=false
      }
    }
    delete state.form_data.basic_info.lights[light_type]
  },
  increase_light_counter (state, payload) {
    var objectKey=state.form_data.basic_info.lights[payload.index];
    objectKey.counter++
  },
  decrease_light_counter (state, payload) {
    var objectKey=state.form_data.basic_info.lights[payload.index];

    if(objectKey.counter > 1){
      objectKey.counter--
    }    
  },
  set_application_data (state, payload) {
    state.form_data.basic_info=payload

    for (var key in state.form_data.basic_info ) {

      if(key === 'video'){
        state.form_data.basic_info[key].url=state.form_data.basic_info[key].url;
        state.form_data.basic_info[key].buffer=state.form_data.basic_info[key].buffer;
        //state.form_data.basic_info[key].video=state.form_data.basic_info[key].video.trim();
      }
      else if(typeof state.form_data.basic_info[key] == 'string' ){
        state.form_data.basic_info[key]=(state.form_data.basic_info[key]).trim();
      }
    }
  },
  set_notification_data (state, payload) {
    state.notification_data=payload
  }
}